var $body;
var profile;

$(function(){
	addEventListeners();
	initializeDOB();
	getBadgeCount();
});

function addEventListeners(){ 
	$('#scan').on('click', function(){
		nativeInterface.scan("earn");
	}); 
	$('#register').on('click', function(){
		nativeInterface.scan("register");
	});
	$('#redeem').on('click', function(){
		nativeInterface.scan("redeem")
	});

	$('#home .settings-btn').on('click', function(){
		nativeInterface.settings();
	});

	$('#campaignList ul').on('click', modalShow);

	$('#syncReg').on('click', function(){ nativeInterface.syncLocallyStoreData("pointtable") });
}

function getBadgeCount(){ // pointtable : registertable
	var count = nativeInterface.getEarnedCount("pointtable");
	if (typeof count != "undefined" && count != 0 && count != null && count != "") {
		document.querySelector('#syncReg').setAttribute('data-rec', count);
		$('#syncReg').addClass('active-sync');
	}else{
		document.querySelector('#syncReg').setAttribute('data-rec', 0);
		$('#syncReg').prop('display', true);
		$('#syncReg').removeClass('active-sync');
	}
}

function scanCallback(type, data){
	if (type == "earn") {
		showModal(data);
	} else if (type == "register"){
		registrationCallback(data)
	}else if (type == "redeem"){
		loginCallback(data)
	}
}

function showModal(){
	$('#scanModal .close').off('click');
	$('#scanModal #submit').off('click');

	$('#scanModal input').val('');

	$('#scanModal').addClass('active-modal');

	$('#scanModal .close').on('click', function(){
		$('#scanModal').removeClass('active-modal');
	});

	$('#scanModal #submit').on('click', function(){
		var amount = $('#amount').val();

		if(isNaN(parseInt(amount))){
			$("#scanModal #amount").focus();
			console.log(amount)
			nativeInterface.msgBox('Amount is invalid!','Invalid Input!');
		}
		else{
			console.log(parseInt(amount).toFixed(2));
			nativeInterface.earn(parseInt(amount).toFixed(2));
		}
	});

}

function earnCallback(amount){
	$('#prompt #ok').off('click');
	$("#scanModal").removeClass('active-modal');
	$("#modal").removeClass('choice').addClass('prompt');
	$("#modal #prompt .image").addClass('active');
	$("#modal #prompt .msg").html("You have successfully earned PHP" + amount);
	$('#prompt #ok').on('click', modalHide);
	// setTimeout(function(){
	// 	modalHide();
	// }, 2000)
	$('.campaign').removeClass('active-view');
	getBadgeCount("pointtable");
}

function registrationCallback(data){
	$('#createAccount').off('click');
	$('#registerPage .close').off('click');

	var memberID = data.memberID

	$('#card').val(data.qrCard);
	$('#fname').val(data.fname);
	$('#lname').val(data.lname);
	$('#email').val(data.email);

	if(data.gender != ''){
		$('#gender').val(data.gender);
		$('#gender').css({"color":"#000","font-family":"Arial","font-style":"normal"});
	}

	$('#mobile').val(data.mobileNum);
	
	if (data.dateOfBirth != "" && data.dateOfBirth != "0000-00-00" && data.dateOfBirth != null){
		dob = data.dateOfBirth.split("-");
		$("#year option[value='"+dob[0]+"']").prop("selected","true");
		$("#day option[value='"+((dob[2].charAt(0) == '0') ? dob[2].substr(1) : dob[2] )+"']").prop("selected","true").addClass('disable');
		$("#month option[value='"+((dob[1].charAt(0) == '0') ? dob[1].substr(1) : dob[1] )+"']").prop("selected","true").addClass('disable');
		document.getElementById("year").style.pointerEvents = "none";
		document.getElementById("month").style.pointerEvents = "none";
		document.getElementById("day").style.pointerEvents = "none";
	}

	$('#home').removeClass('active-view');
	$('#registerPage').addClass('active-view');
	
	$('#gender').on('change', function(){
		if($('#gender').val() != 'Gender'){
			$('#gender').css({"color":"#000","font-family":"Arial","font-style":"normal", "font-size":"16px"});
		}else{
			$('#gender').css({"color":"#bdbdbd","font-family":"Sports","font-style":"italic"});
		}
	});
	
	$('#registerPage.view .close').on('click', function(){
		$('#registerPage').removeClass('active-view');
		$('#home').addClass('active-view');
	});

	$('#createAccount').on('click',function(){
		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var email = $('#email').val();
		var gender = $('#gender').val();
		var mobile = $('#mobile').val();
		var year = $('#year').val();
		var day = $('#day').val();
		var month = $('#month').val();

		$('input').blur();

		if(!validateEmail(email) && email != ''){
			nativeInterface.msgBox("Please enter a valid email address!", "Invalid Input!");
			$('#email').focus();
		}else{
			var data = [{
				memberID: memberID,
				email: email,
				mobileNum: mobile,
				gender: gender,
				fname: fname,
				lname: lname,
				dateOfBirth: year+"-"+month+"-"+day
			}];
			console.log(data);
			nativeInterface.updateProfile(JSON.stringify(data));
		}
	});
}

function loginCallback(jsonData){
	$('#logout').off('click');
	profile = jsonData;

	$('#home').removeClass('active-view');
	$('#redeemPage').addClass('active-view');

	if( profile.totalPoints == '' || profile.totalPoints == null){
		$('#myPoints').html(0);
	}else{
		$('#myPoints').html(profile.totalPoints);
	}

	$('#cardNum').html(profile.qrCard);
	$('#campaignWrapper #preloader').removeClass('hide');
	$('#campaignList').addClass ('hide');

	nativeInterface.getCampaignList(false)

	$('#logout').on('click', function(){
		$('#redeemPage').removeClass('active-view');
		$('#home').addClass('active-view');
	});

}

function listCallback(a,data){
	$('#campaignWrapper #preloader').addClass('hide');
	$('#empty').addClass('hide');
	$('#campaignList').removeClass('hide');

	var jsonData = data;
	var content = "";

	if(a=="campaign"){
		for (var i = 0; i < jsonData.length; i++) {
			content+='<li data-title="'+encodeURIComponent(jsonData[i].name)+'" data-points="'+jsonData[i].points+'" data-id="'+jsonData[i].loyaltyID+'" class="'+(( profile.totalPoints >= jsonData[i].points)? "redeem": "")+'">'+
								'<div class="points">'+'<div>'+jsonData[i].points+'</div>'+'</div>'+
								'<div class="name">'+jsonData[i].name+'</div>'+
								'</li>'
		}
	}
	$('#campaignList ul').html(content);
}

function errorCallback(){
	$('#empty').off('click');
	$('#campaignWrapper #preloader').addClass('hide');
	$('#empty').removeClass('hide');
	$('#empty').on('click', function(){
		nativeInterface.getCampaignList(true)
	});
}

function modalShow(e){
	$('#choice #cancel').off('click');
	$('#choice #ok').off('click');
	if (e.target != e.currentTarget) {
		var title = e.target.getAttribute('data-title');
		var points = e.target.getAttribute('data-points');
		var id = e.target.getAttribute('data-id');
		
		$('#modal').removeClass().addClass('active-modal');
		if($(e.target).hasClass('redeem') || $(e.target).hasClass('freq')){
			$('#choice .msg').html("Are you sure you want to redeem " +decodeURIComponent(title) + "?");
			$('#modal').addClass('choice');	
			$('#choice #ok')[0].setAttribute('data-points', points);
			$('#choice #ok')[0].setAttribute('data-title', title);
			$('#choice #ok')[0].setAttribute('data-id', id);
		}else{
			$('#warning .msg').html("You need more points to redeem " +decodeURIComponent(title));
			$('#modal').addClass('warning');	
		}
	}

	$('#choice #cancel').on('click', modalHide);

	$('#choice #ok').on('click', redeemItem); 

	$('#warning #ok').on('click', modalHide);
}

function redeemItem(e){
	e.preventDefault();
	nativeInterface.redeem(e.target.getAttribute('data-id'),decodeURIComponent(e.target.getAttribute('data-title')), e.target.getAttribute('data-points'));
	e.stopPropagation();
}

function redeemCallback(data,loyaltyName,redeemPoints){
	$('#prompt #ok').off('click');
	$("#modal").removeClass('choice').addClass('prompt');
	$("#modal #prompt .image").addClass('active');
	$("#modal #prompt .msg").html("You have successfully redeemed " + loyaltyName);

	$('#prompt #ok').on('click', modalHide);
	// setTimeout(function(){
	// 	modalHide();
	// }, 2000)
	$('.campaign').removeClass('active-view');
	loginCallback(data);
}

function modalHide(){
	$('#modal').removeClass().addClass('modal');
}

function initializeDOB(){ // INITIALIZE DATE OF BIRTH SELECTION (selectors: #days, #months, #year)
	$('#registerPage.view #year').html('<option value="0000" selected>Year</option>');
	$('#registerPage.view #month').html('<option value="00" selected>Month</option>');

	for (i = new Date().getFullYear(); i > 1939; i--){
		var currentYear = new Date();
		$('#year').append($('<option />').val(i).html(i));   
	}

	for (i = 1; i < 13; i++){
		var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
		$('#month').append($('<option />').val(i).html(mon[i-1]));
	}

	updateNumberOfDays(); 

	$('#year, #month').change(function(){
		updateNumberOfDays(); 
	});

	if($('#year, #month, #day').val() == ""){
		$('#year, #month, #day').css('color','#bdbdbd');
	}

	$('#year').on('change', function(){
			var year = new Date().getFullYear();
			if($('#year').val() != ""){
				$('#year').css({"color":"#000","font-family":"Arial","font-style":"normal"});
			}else{
				$('#year').css({"color":"#bdbdbd","font-family":"Sports","font-style":"italic"});
			}
			if($('#year').val() == year){
				updateMonth();
			}
	});
	$('#month').on('change', function(){
			if($('#month').val() != ""){
				$('#month').css({"color":"#000","font-family":"Arial","font-style":"normal"});
			}else{
				$('#month').css({"color":"#bdbdbd","font-family":"Sports","font-style":"italic"});
			}
			updateNumberOfDays();
	});
	$('#day').on('change', function(){
			if($('#day').val() != ""){
				$('#day').css({"color":"#000","font-family":"Arial","font-style":"normal"});
			}else{
				$('#day').css({"color":"#bdbdbd","font-family":"Sports","font-style":"italic"});
			}
	});
}

function updateNumberOfDays(){ // CHANGE HANDLER FOR DATE OF BIRTH
	month=$('#month').val();
	year=$('#year').val();
	day=$('#day').val();
	days=daysInMonth(month, year);

	$('#registerPage.view #day').html('<option value="00">Day</option>');


	for(i=1; i < days+1 ; i++){
		if((day!='' && month!='') && (((days < day) && (i == days)) || i==day)){
			$('#day').append($('<option selected />').val(i).html(i));
		}else{
			$('#day').append($('<option />').val(i).html(i));
		}
	}
}

function updateMonth(){
	$('#registerPage.view #month').html('<option value="00">Month</option>').off('change');
	var monthToday = new Date().getMonth();
	var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
	
	$('#registerPage.view #month').html('<option value="">Month</option>');
	for (i = 1; i <= new Date().getMonth()+1; i++){
	    $('#registerPage.view #month').append($('<option />').val(i).html(mon[i-1]));
	}

	$('#year').on('change',function(){
		$('#registerPage.view #month').html('<option value="00">Month</option>');	
		for (i = 1; i < 13; i++){
			var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
			$('#month').append($('<option />').val(i).html(mon[i-1]));
		}
	});
}
 
function daysInMonth(month, year) {
	var d = new Date(year, month, 0).getDate();
	return d;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}